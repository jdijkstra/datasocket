/*global define, angular */

require([ 'angular', 'angular-animate', 'underscorejs', 'bootstrap',
    'ui-bootstrap', 'ui-bootstrap-tpls', './datasocket' ], function(angular) {
  'use strict';

  var module = angular.module('datasocket-console', [ 'ngAnimate',
      'ui.bootstrap', 'datasocket' ]);

  module.controller('ConsoleCtrl', [ '$scope', '$parse', 'focus',
      function($scope, $parse, focus) {
        $scope.url = localStorage.getItem('url') || 'ws://localhost:9000/';

        $scope.socket = false;

        $scope.log = [];

        var logMessage = function(sender, msg, apply) {
          var logIt = function() {
            $scope.log.push({
              sender : sender,
              msg : msg
            });
            $scope.log = $scope.log.slice(-30);
          };
          if (apply) {
            $scope.$apply(logIt);
          } else {
            logIt();
          }
        };

        $scope.connect = function(url) {
          localStorage.setItem('url', url);
          $scope.socket = new WebSocket(url);
          $scope.socket.onopen = function() {
            logMessage('system', 'Connection opened', true);
          };
          $scope.socket.onmessage = function(msg) {
            logMessage('server', angular.fromJson(msg.data), true);
          };
          $scope.socket.onerror = function(msg) {
            logMessage('system', msg, true);
          };
          $scope.socket.onclose = function() {
            logMessage('system', 'Connection closed', true);
            $scope.$apply(function() {
              $scope.socket = false;
            });
          };
          focus('focusCommand');
        };

        $scope.disconnect = function() {
          $scope.socket.close();
        };

        $scope.exec = function(command) {
          if (command == 'help') {
            logMessage('system', {
              '{limit: 10}' : 'Set the size of the current window to 10',
              '{offset: 0}' : 'Set the start of the current window to 0',
              "{filter: {name: 'Someone'}}" : 'Filter data by name'
            }, false);
          } else {
            if (command.charAt(0) == '{') {
              command = $parse(command)();
            }
            logMessage('client', command, false);
            $scope.socket.send(angular.toJson(command));
          }
          $scope.command = '';
          focus('focusCommand');
        };
      } ]);
  angular.bootstrap(document, [ 'datasocket-console' ]);
});