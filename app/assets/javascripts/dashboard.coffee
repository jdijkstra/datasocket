### global define, angular ###

require [ 'angular', 'angular-animate', 'underscorejs', 'bootstrap',
    'ui-bootstrap', 'ui-bootstrap-tpls', './datasocket' ], (angular) ->
  'use strict'

  module = angular.module('datasocket-dashboard', [ 'ngAnimate',
      'ui.bootstrap', 'datasocket' ])
  
  module.config(['SettingsProvider', (SettingsProvider) ->
    SettingsProvider.debugLogging = true
  ])

  module.controller('DashboardCtrl', [ '$scope', '$parse', 'Collection',
    ($scope, $parse, Collection) ->
      
  ])
     
  angular.bootstrap document, ['datasocket-dashboard']
