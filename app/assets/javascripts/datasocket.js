/*global define, angular */

define(function(require) {
  var angular = require('angular');
  require('./datasocket/model');
  require('./datasocket/directives');

  var module = angular.module('datasocket', ['datasocket.core', 'datasocket.model', 'datasocket.directives']);

  var core = angular.module('datasocket.core', []);
  
  module.filter('reverse', function() {
    return function(items) {
      return items.slice().reverse();
    };
  });

  module.directive('focusOn', function() {
    return function(scope, elem, attr) {
      scope.$on('focusOn', function(e, name) {
        if (name === attr.focusOn) {
          elem[0].focus();
        }
      });
    };
  });

  module.factory('focus', [ '$rootScope', '$timeout',
      function($rootScope, $timeout) {
        return function(name) {
          $timeout(function() {
            $rootScope.$broadcast('focusOn', name);
          });
        };
      } ]);

  core.provider('Settings', function() {
    this.$get = ['$window', function($window) {
      var settings = {
        debugLogging: false,
        root: 'ws://' + $window.location.host + '/ds/'
        // default settings go here
      };
      
      // collect the values that might have been changed in module.config()
      angular.forEach(settings, function(value, key) {
        if (angular.isDefined(this[key])) {
          settings[key] = this[key];
        }
      }, this);
      return settings;
    }];
  });
});
