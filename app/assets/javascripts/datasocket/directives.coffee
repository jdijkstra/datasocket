###global define, angular###

define (require) ->
  angular = require 'angular'
  require 'ui-bootstrap'
  require 'ui-bootstrap-tpls'

  module = angular.module('datasocket.directives', ['datasocket.core', 'datasocket.model', 'ui.bootstrap', 'ui.bootstrap.tpls'])

  module.directive('dsCollection', ['Collection', (Collection) ->
    restrict: 'A'
    scope:
      dsCollection: '@'
      options: '='
    transclude: true
    # controller: ->
    link: (scope, element, attrs, controller, transclude) ->
      scope.collection = new Collection(scope, scope.dsCollection)
      scope.collection.select(scope.options ? {})
      # let the transcluded html use this directive's scope (http://angular-tips.com/blog/2014/03/transclusion-and-scopes/)
      transclude(scope, (clone, scope) -> element.append(clone))
  ])

  module.directive('dsPaginator', ['Settings', (Settings) ->
    require: '^dsCollection'
    restrict: 'E'
    scope: true
    template: '<pagination total-items="collection.info.total" ng-model="pageNumber" ng-change="pageChanged()" items-per-page="collection.info.limit" class="pagination-sm" max-size="10" rotate="false"></pagination>'
    replace: false
    controller: ['$scope', ($scope) ->
      console.log('paginator', $scope.pageNumber, $scope.$parent.collection) if Settings.debugLogging
      $scope.collection = $scope.$parent.collection
      $scope.pageNumber ?= 1
      $scope.$watch((-> $scope.collection.info.offset), ->
        console.log('page', ($scope.collection.info.offset // $scope.collection.info.limit) + 1) if Settings.debugLogging
        if $scope.collection.info.offset?
          $scope.pageNumber = ($scope.collection.info.offset // $scope.collection.info.limit) + 1
      )
      $scope.pageChanged = ->
        console.log('page changed', $scope.pageNumber) if Settings.debugLogging
        if $scope.collection.info.limit?
          $scope.collection.select(
            offset: ($scope.pageNumber - 1) * $scope.collection.info.limit
          )
    ]
  ])
