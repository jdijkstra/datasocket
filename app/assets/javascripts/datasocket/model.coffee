###global define, angular###

define (require) ->
  angular = require 'angular'

  module = angular.module('datasocket.model', [])

  module.factory('Collection', ['$timeout', '$q', 'Settings', 'Item',
    ($timeout, $q, Settings, Item) ->
      handleMessage = (collection, message) ->
        collection.scope.$apply(->
          if message.item?
            collection._deferredInfo.promise.then((info) ->
              if message.index >= info.offset and message.index < info.offset + info.limit
                item = new Item(collection, message.item)
                if collection.index[message.item.id]?
                  collection.buffer[collection.index[message.item.id]] = item
                else
                  collection.buffer.push(item)
                  collection.index[message.item.id] = collection.buffer.length - 1
            )
          if message.count?
            console.log('received metadata') if Settings.debugLogging
            collection.info = message
            collection._deferredInfo.resolve(message)
        )

      class Collection
        constructor: (@scope, @name) ->
          self = this
          @url = Settings.root + 'collection/' + name
          @socket = $q((resolve, reject) ->
            socket = new WebSocket(self.url)
            socket.onopen = ->
              resolve socket
            socket.onclose = ->
              reject 'Connection closed'
            socket.onerror = ->
              reject error
          )
          @refreshMeta()
          @buffer = [];
          @index = {};
          @info = {};
          @socket.then((socket) ->
            socket.onmessage = (message) ->
              handleMessage(self, angular.fromJson(message.data))
          )

        sendCommand: (command) ->
          @socket.then((socket) ->
            cmd = angular.toJson(command)
            console.log('sendCommand', cmd) if Settings.debugLogging
            socket.send(cmd)
          )

        refreshMeta: ->
          @_deferredInfo = $q.defer()
          @info = {}
          @sendCommand 'info'

        refresh: ->
          @refreshMeta()
          @sendCommand 'fetch'
        
        select: (options) ->
          self = this;
          @socket.then(->
            self._deferredInfo.promise.then((info) ->
              if options?
                self.info = angular.extend(info, options)
              else
                options =
                  offset: info.offset
                  limit: info.limit
              self.buffer = []
              self.index = {}
              self.sendCommand(options)
            )
          )

      return Collection
  ]);

  module.factory('Item', ['Field', (Field) ->
    class Item
      constructor: (collection, item) ->
        @_collection = collection;
        @fields = [];
        angular.forEach(item, (value, key) ->
          field = new Field(this, key, value)
          @fields.push(field)
          @[key] = field
        , this)
    
      _dirtyFields: ->
        field for field in @fields when field.dirty

      _isDirty: ->
        @_dirtyFields().length > 0

      _save: ->
        dirtyFields = @_dirtyFields()
        if dirtyFields.length > 0
          setter = {}
          for field in dirtyFields
            setter[field.name] = field.value
            field._onSave()
          @_collection.sendCommand(
            save:
              id: @id._originalValue
              $set: setter
          )
    
    return Item
  ])

  module.factory('Field', [->
    class Field
      constructor: (item, @name, value) ->
        @_item = item
        @_originalValue = value
        @value = angular.copy(value)
        @dirty = false
        self = this
        item._collection.scope.$watch((-> self.value), (-> self.dirty = not angular.equals(self.value, self._originalValue)))

      save: ->
        setter = {}
        setter[@name] = @value
        @_item._collection.sendCommand(
          save:
            id: @_item.id._originalValue
            $set: setter
        )
        @_onSave()
        true

      _onSave: ->
        @_originalValue = @value
        @dirty = false

    return Field
  ])
