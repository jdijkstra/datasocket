package datasocket

import play.api._
import play.api.libs.concurrent.Akka
import akka.actor._
import datasocket.actors._

class DataSocket(implicit app: Application) extends Plugin {

  lazy val metaDataActor = Akka.system.actorOf(Props[MetaDataActor], "metadata-actor")
  
}

object DataSocket {
  
  def plugin = Play.current.plugin[DataSocket]
      .getOrElse(throw new RuntimeException("DataSocket plugin not loaded"))
  
  def metaDataActor: ActorRef = plugin.metaDataActor
  
}