package datasocket
package actors

import akka.actor._
import play.api.libs.json._
import akka.pattern.ask
import akka.util.Timeout
import scala.concurrent.duration._
import play.api.libs.concurrent.Execution.Implicits._
import play.modules.reactivemongo.json.collection.JSONCollection
import play.api.libs.iteratee.Enumeratee
import play.api.libs.iteratee.Iteratee
import datasocket.lib.User
import play.api.libs.json.monad.syntax._
import play.api.libs.json.extensions._
import scala.util.Failure
import scala.util.Success

object CollectionActor {

  def props(user: Option[User], metaData: CollectionMetaData, collection: JSONCollection)(out: ActorRef) = {
    Props(new CollectionActor(user, metaData, collection, out))
  }

}

abstract sealed class Permission
object Permission {
  def parse(p: String): Option[Permission] = p match {
    case "read" => Some(Read)
    case "write" => Some(Write)
    case _ => None
  }
}
case object Read extends Permission
case object Write extends Permission

class CollectionActor(user: Option[User], metaData: CollectionMetaData, collection: JSONCollection, out: ActorRef) extends Actor {

  implicit val timeout = Timeout(10 seconds)

  val writerWithId = Writes[JsObject] {
    _.updateAllKeyNodes {
      case (_ \ "_id", value) => ("id" -> value \ "$oid")
    }
  }
  
  val writerInverseId = Writes[JsObject] {
    _.updateAllKeyNodes {
      case (_ \ "id", value) => ("_id" -> Json.obj("$oid" -> value))
    }
  }

  val name = metaData.name

  var filter = Json.obj()

  var projection: Option[JsObject] = None

  var order = Json.obj()

  var window = (0, 20)

  val userId = user.map(_.email).getOrElse("anonymous")

  val groups = user.map(_.groups).getOrElse(List("anonymous"))

  val permissions: Set[Permission] = {
    if (groups contains "admin") {
      Set(Read, Write)
    } else {
      (for (authItem <- metaData.auth) yield {
        val authItemPermissions: Set[Permission] = if (authItem.user.map(_ == userId).getOrElse(false) || authItem.group.map(g => groups.contains(g)).getOrElse(false)) {
          Set(authItem.read match {
            case Some(true) => Some(Read)
            case _ => None
          }, authItem.write match {
            case Some(true) => Some(Write)
            case _ => None
          }).flatten
        } else {
          Set()
        }
        authItemPermissions
      }).flatten.toSet
    }
  }

  val defaultFilter = permissions.contains(Read) match {
    case true =>
      // we already have read access for all records
      Json.obj()
    case false =>
      Json.obj(
        "$or" -> Json.arr(
          // you can always read your own records
          Json.obj(
            "creator" -> userId),
          // use record level auth
          Json.obj(
            "auth" -> Json.obj(
              "$elemMatch" -> Json.obj(
                "user" -> userId,
                "read" -> true))),
          Json.obj(
            "auth" -> Json.obj(
              "$elemMatch" -> Json.obj(
                "group" -> Json.obj(
                  "$in" -> groups),
                "read" -> true)))))

  }
  
  def safeFilter = filter ++ defaultFilter 

  def receive = {

    case JsObject(operations) =>
      val updates = for ((operation, payload) <- operations) yield {
        (operation, payload) match {
          case ("filter", f: JsObject) =>
            filter = writerInverseId.writes(f).as[JsObject]
            true
          case ("offset", JsNumber(o)) =>
            window = (o.toInt, window._2)
            true
          case ("order", o: JsObject) =>
            order = o
            true
          case ("limit", JsNumber(l)) =>
            window = (window._1, l.toInt)
            true
          case ("project", o: JsObject) =>
            projection = Some(o)
            true
          case ("project", JsUndefined()) =>
            projection = None
            true
          case ("fetch", props: JsObject) =>
            val offset = (props \ "offset").asOpt[Int] match {
              case Some(o) => o
              case None => window._1
            }
            val limit = (props \ "limit").asOpt[Int] match {
              case Some(l) => l
              case None => window._2
            }
            window = (offset, limit)
            true
          case ("save", doc: JsObject) =>
            if (permissions contains Write) {
              (doc \ "id").validate[String] match {
                case JsSuccess(id, _) =>
                  // update
                  // make sure you can only update records you have access to
                  val select = Json.obj("_id" -> Json.obj("$oid" -> id)) ++ defaultFilter
                  // strip the id field
                  val record = doc.transform((__ \ 'id).json.prune).get
                  collection.update(select, record) onComplete {
                    case Failure(e) =>
                      out ! Json.obj("error" -> e.getMessage())
                    case Success(lastError) =>
                      out ! Json.obj("success" -> lastError.message) 
                  }
                case _ =>
                  // insert
                  collection.insert(doc ++ Json.obj("creator" -> userId)) onComplete {
                    case Failure(e) =>
                    case Success(lastError) =>
                  }
              } 
            } else {
              out ! Json.obj("error" -> "You can't make me save that!")
            }
            false
          case (o, _) =>
            out ! Json.obj("error" -> ("Unknown operation: " + o))
            false
        }
      }
      if (updates.toList.exists(x => x)) {
        sendCurrentWindow
      }

    case JsString("info") =>
      for {
        count <- DataSocket.metaDataActor ? Count(name, Some(safeFilter))
        total <- DataSocket.metaDataActor ? Count(name, Some(defaultFilter))
      } {
        out ! writerWithId.writes(Json.obj("me" -> Json.toJson(user),
          "permissions" -> JsArray(permissions.map(p => JsString(p.toString)).toSeq),
          "filter" -> filter,
          "order" -> order,
          "offset" -> JsNumber(window._1),
          "limit" -> JsNumber(window._2),
          "count" -> JsNumber(count.asInstanceOf[Int]),
          "total" -> JsNumber(total.asInstanceOf[Int])))
      }

    case JsString("fetch") =>
      sendCurrentWindow

    case x =>
      out ! Json.obj("error" -> "Unknown message", "input" -> x.toString())
  }

  def sendCurrentWindow = {
    println(safeFilter.toString)
      val enumerator = collection.find(safeFilter, lib.Security.safeProjection(projection))
        .sort(order)
        .cursor[JsObject]
        .enumerate()
      enumerator &> Enumeratee.drop(window._1) &> Enumeratee.take(window._2) run Iteratee.fold(window._1) {
        case (index, item) =>
          out ! JsObject(Seq("item" -> writerWithId.writes(item), "index" -> JsNumber(index)))
          index + 1
    }
  }

}