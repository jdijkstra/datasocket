package datasocket
package actors

import akka.actor._
import play.api.Play.current
import play.api.libs.json._
import play.modules.reactivemongo.ReactiveMongoPlugin
import play.modules.reactivemongo.json.collection.JSONCollection
import play.modules.reactivemongo.json.BSONFormats
import reactivemongo.bson.BSONDocument

case class AuthMetaData(group: Option[String], user: Option[String], read: Option[Boolean], write: Option[Boolean])

object AuthMetaData {
  implicit val format = Json.format[AuthMetaData]
}

case class CollectionMetaData(name: String, auth: List[AuthMetaData])

object CollectionMetaData {
  implicit val format = Json.format[CollectionMetaData]
}

case class Login(email: String, password: String)
case class FetchUser(email: String)
case class Count(collection: String, filter: Option[JsObject])
case class Exists(collection: String)

class MetaDataActor extends Actor {

  import reactivemongo.api._
  import scala.concurrent.ExecutionContext.Implicits.global
  import lib.User
  
  def coll(name: String) = ReactiveMongoPlugin.db.collection[JSONCollection](name)
  
  def collection = coll("datasocket-collections")
  def userCollection = coll("datasocket-users")
  
  def receive = {
    case Login(email, password) =>
      val originalSender = sender
      for (user <- userCollection.find(Json.obj("email" -> email, "password" -> lib.Security.hash(password, email)), lib.Security.safeProjection()).cursor[JsObject].headOption) {
        originalSender ! user.map(_.as[User]) 
      }
    
    case FetchUser(email) =>
      val originalSender = sender
      for (user <- userCollection.find(Json.obj("email" -> email), lib.Security.safeProjection()).cursor[JsObject].headOption) {
        originalSender ! user 
      }
      
    case Exists(name) =>
      val originalSender = sender
      for (head <- collection.find(Json.obj("name" -> name)).cursor[JsObject].headOption) {
        originalSender ! head.isDefined
      }
      
    case Count(name, filter) =>
      val originalSender = sender
      for (count <- ReactiveMongoPlugin.db.command(reactivemongo.core.commands.Count(name, filter.map(f => BSONFormats.toBSON(f).get.asInstanceOf[BSONDocument])))) {
        originalSender ! count
      }
    
    case name: String =>
      val originalSender = sender
      for (head <- collection.find(Json.obj("name" -> name)).cursor[JsObject].headOption) {
        originalSender ! head.map { metadata =>
          metadata.validate[CollectionMetaData] match {
            case JsSuccess(collectionMeta, _) =>
              collectionMeta -> ReactiveMongoPlugin.db.collection[JSONCollection](name)
            case _ =>
              "invalid metadata"
          }
        }
      }
  }
  
}