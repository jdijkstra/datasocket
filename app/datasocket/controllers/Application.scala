package datasocket
package controllers

import scala.concurrent.Future
import play.api._
import play.api.mvc._
import play.api.libs.json._
import play.api.Play.current
import akka.pattern.ask
import akka.util.Timeout
import scala.concurrent.duration._
import play.api.libs.concurrent.Execution.Implicits._
import datasocket.actors._
import play.modules.reactivemongo.json.collection.JSONCollection
import datasocket.lib.User
import play.api.libs.functional.syntax._

object Application extends Controller {

  implicit val timeout = Timeout(10 seconds)
  
  def console = Action {
    Ok(datasocket.views.html.console())
  }
  
  def dashboard = Action {
    Ok(datasocket.views.html.dashboard())
  }
  
  val loginRequest: Reads[(String, String)] = 
    (JsPath \ "email").read[String] and
    (JsPath \ "password").read[String] tupled
  
  def login = Action.async(parse.json) { implicit request =>
    loginRequest.reads(request.body) match {
      case JsSuccess((email, password), _) =>
        for (maybeUser <- lib.Security.checkLogin(email, password)) yield {
          maybeUser match {
            case Some(user) =>
              Ok(Json.toJson(user)).withSession("email" -> email)
            case _ =>
              Unauthorized(JsString("Unknown user or wrong password"))
          }
        }
      case _ =>
        Future.successful(BadRequest(JsString("Invalid format")))
    }
  }
  
  def logout = Action {
    Ok.withNewSession
  }
  
  private def getUser(user: Option[_]): Option[User] = {
    user.asInstanceOf[Option[JsObject]].flatMap(u => User.format.reads(u) match {
      case JsSuccess(user, _) => Some(user)
      case _ => None
    })
  }
  
  def collection(name: String) = WebSocket.tryAcceptWithActor[JsValue, JsValue] { request =>
    Logger.info("Collection requested: " + name)
    for {
      collection <- (DataSocket.metaDataActor ? name)
      user <- (DataSocket.metaDataActor ? FetchUser(request.session.get("email").getOrElse("anonymous")))
    } yield {
      (collection, user) match {
        case (Some((metadata: CollectionMetaData, coll: JSONCollection)), user: Option[_]) =>
          Logger.info("Collection found")
          Right(CollectionActor.props(getUser(user), metadata, coll))
        case _ =>
          Logger.info("No collection found")
          Logger.info(collection.toString())
          Logger.info(user.toString())
          Left(NotFound)
      }
    }
  }

}