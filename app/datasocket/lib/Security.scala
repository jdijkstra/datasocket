package datasocket
package lib

import play.api.libs.json._
import actors.Login
import akka.pattern.ask
import akka.util.Timeout
import scala.concurrent.duration._
import play.api.libs.concurrent.Execution.Implicits._

case class User(email: String, password: Option[String], name: Option[String], groups: List[String])

object User {
  implicit val format = Json.format[User]
}

object Security {

  implicit val timeout = Timeout(10 seconds)
  
  def hash(value: String, salt: String) = {
    def hashOnce(value: String) = {
      val md = java.security.MessageDigest.getInstance("SHA-256")
      javax.xml.bind.DatatypeConverter.printHexBinary(md.digest(value.getBytes("UTF-8")))
    }

    (1 to 10000).toList.foldLeft(value) { case (hash, _) => hashOnce(hash + salt) }
  }
  
  def checkLogin(email: String, password: String) = {
    for (user <- DataSocket.metaDataActor ? Login(email, password)) yield {
      user.asInstanceOf[Option[User]]
    }
  }

  def safeProjection(projection: Option[JsObject] = None) =
    projection.getOrElse(Json.obj()) ++ Json.obj("auth" -> 0, "password" -> 0)

}