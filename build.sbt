name := """data-socket"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.11.1"

resolvers += "Sonatype Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots/"

libraryDependencies ++= Seq(
  cache,
  ws,
  "org.reactivemongo" %% "play2-reactivemongo" % "0.10.5.akka23-SNAPSHOT",
  "com.mandubian"     %% "play-json-zipper"    % "1.2",
  "org.webjars" %% "webjars-play" % "2.3.0",
  "org.webjars" % "jquery" % "2.1.1",
  "org.webjars" % "bootswatch-superhero" % "3.2.0-1",
  "org.webjars" % "angularjs" % "1.3.0-beta.17",
  "org.webjars" % "angular-ui-bootstrap" % "0.11.0-2",
  "org.webjars" % "underscorejs" % "1.6.0-3"
)

includeFilter in (Assets, LessKeys.less) := "*.less"

pipelineStages := Seq(rjs, digest, gzip)